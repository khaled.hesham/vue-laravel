import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'

Vue.use(VueRouter)

const routes = [
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard
    }
]

export const router = new VueRouter({
    mode: 'history',
    routes
})
